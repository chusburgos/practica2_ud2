package gui;

import javax.swing.*;

public class OptionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;

    public OptionDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
    }

    public static void main(String[] args) {
        OptionDialog dialog = new OptionDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
