package base.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboGenero de la vista.
 * Representan las marcas de zapatos que existen.
 */

public enum MarcasZapatos {

    NIKE("Nike"),
    ADIDAS("Adidas"),
    CONVERSE("Converse"),
    VANS("Vans"),
    PUMA("Puma"),
    REEBOK("Reebok"),
    MUNICH("Munich"),
    SALOMON("Salomon"),
    FILA("Fila"),
    JORDAN("Jordan"),
    DOCTONMARTINS("Doctor Martins"),
    POLO("Polo"),
    TOMMYHILFIGUER("Tommy Hilfiguer"),
    GUESS("Guess"),
    DIOR("Dior"),
    CHANEL("Chanel"),
    VALENTINO("Valentino"),
    GIORGIOARMANI("Giorgio Armani"),
    VERSACE("Versace"),
    BULBERRY("Bulberry"),
    LOUISVUITTON("Louis Vuitton"),
    PRADA("Prada"),
    GUCCI("Gucci"),
    BALENCIAGA("Balenciaga");

    private String valor;

    MarcasZapatos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
