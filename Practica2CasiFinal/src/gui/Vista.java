package gui;

import base.enums.MarcasZapatos;
import base.enums.TiposProveedores;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame {

    private final static String TITULOFRAME = "WALKSHOES";
    private JTabbedPane tabbedPane1;
    private JPanel panel1;

    // ZAPATO
    JTextField referenciaTxt;
    JTextField sexoTxt;
    JTextField tallaTxt;
    JComboBox <String> comboMarca;
    JComboBox <String> comboProveedor;
    JTextField precioTxt;
    DateTimePicker fecha;
    JButton anadirBtn;
    JButton eliminarBtn;
    JButton modificarBtn;
    JTable tablaZapatos;

    // MARCA
    JComboBox <String> comboNombreMarca;
    JTextField modeloTxt;
    JTextField colorTxt;
    JButton anadirMarcaBtn;
    JButton eliminarMarcaBtn;
    JButton modificarMarcaBtn;
    JTable tablaMarcas;

    // PROVEEDOR - son las tiendas donde se venden los productos
    JComboBox<String> comboNombreProveedor;
    JTextField direccionTxt;
    JTextField emailTxt;
    JTextField telefonoTxt;
    JTextField webTxt;
    JButton anadirProveedorBtn;
    JButton eliminarProveedorBtn;
    JButton modificarProveedorBtn;
    JTable tablaProveedor;

    // Estado del uso de los botones y las acciones que se estan realizando
    private JLabel etiquetaEstado;

    // Default table models
    DefaultTableModel dtmZapatos;
    DefaultTableModel dtmMarcas;
    DefaultTableModel dtmProveedores;

    //Opciones de la clase DIALOG
    OptionDialog optionDialog;

    //Menu
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    //Guardar los cambios de la clase DIALOG
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase.
     * Devuelve el título de la ventana y llama al metodo que la inicia
     */

    public Vista() {
        super(TITULOFRAME);
        initFrame(optionDialog);
    }


    /**
     * Configura la ventana y la hace visible
     */
    public void initFrame(OptionDialog optionDialog) {
        this.setContentPane(panel1);
        this.setContentPane(tabbedPane1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumCombo();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, coloca la información en sus respectivas tablas
     */
   private void setTableModels() {
        this.dtmZapatos = new DefaultTableModel();
        this.tablaZapatos.setModel(dtmZapatos);

        this.dtmMarcas = new DefaultTableModel();
        this.tablaMarcas.setModel(dtmMarcas);

        this.dtmProveedores = new DefaultTableModel();
        this.tablaProveedor.setModel(dtmProveedores);
   }

    /**
     * Crea la pestaña superior desplegable del menu
     */
    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Selecciona y coloca los parametros de la clase enumerada, el valor, la marca y/o el tipo de proveedor.
     */

    private void setEnumCombo() {
        for(MarcasZapatos constant : MarcasZapatos.values()) {
            comboNombreMarca.addItem(constant.getValor());
        }
        comboNombreMarca.setSelectedItem(-1);

        for (TiposProveedores constant : TiposProveedores.values()) {
            comboProveedor.addItem(constant.getValor());
        }
        comboProveedor.setSelectedItem(-1);
    }

    /**
     * Coloca una ventana, JDialog, para introducir la contreña del administrador y poder configurar la base de datos
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);
        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
