package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;


public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() { getPropValues(); }

    String getIP() {
        return ip;
    }

    String getUser() {
        return user;
    }

    String getPassword() {
        return password;
    }

    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    void conectar() {
        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://" + ip + ":3306/ZapateriaBaseDatos", user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://" + ip + ":3306/ZapateriaBaseDatos", user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatosZapateria.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
    * Inserta los datos de zapatos, todos los parametros
    */
    void insertarZapato(String referencia, String sexo, int talla, String marca, String proveedor,
                        LocalDate fechaEntrada, float precio) {
        String sentenciaSql = "INSERT INTO zapato (referencia, sexo, talla, marca, proveedor, fechaEntrada, precio)" +
                "VALUES(?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;
        int idmarca = Integer.valueOf(marca.split(" ")[0]);
        int idproveedor = Integer.valueOf(proveedor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, referencia);
            sentencia.setString(2, sexo);
            sentencia.setInt(3, talla);
            sentencia.setInt(4, idmarca);
            sentencia.setInt(5, idproveedor);
            sentencia.setDate(6, Date.valueOf(fechaEntrada));
            sentencia.setFloat(7, precio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Inserta los datos de Marca de los zapatos, todos los parametros
     */
    void insertarMarca(String marca, String modelo, String color) {
        String sentenciaSql = "INSERT INTO marca (marca, modelo, color)" +
                " VALUES(?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, color);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }
    /**
     * Inserta los datos de los proveedores de los zapatos, todos los parametros
     */
    void insertarProveedor(String nombre, String direccion, String email, int telefono, String web) {
        String sentenciaSql = "INSERT INTO proveedor(nombre, direccion, email, telefono, web)" +
                "VALUES(?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, email);
            sentencia.setInt(4, telefono);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
            }

    }


    /**
     * Lee el archivo de propiedades y devuelve los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
