package gui;

import com.github.lgooddatepicker.components.DatePicker;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;


public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() { getPropValues(); }

    String getIP() { return ip; }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Carga los datos desde un fichero
     */
    private Connection conexion;

    void conectar() {
        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://" + ip + ":3306/ZapateriaBaseDatos", user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://" + ip + ":3306/ZapateriaBaseDatos", user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatosZapateria.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }


    /*Inserta los datos de zapatos, todos los parametros
     *   @param referencia del zapato, tendra letras y numeros
     *   @param sexo para saber si sera de hombre o de mujer el zapato
     *   @param talla numero de talla del cliente
     *   @param marca marca que se elegira con el combobox
     *   @param proveedor el proveedor que hay en la clase enumerada, tambien se elegira con el combobox
     *   @param fechaEntrada la entrada del zapato en la empresa
     *   @param precio el precio del calzado
     *   @param idzapato al igual que la referencia, se usara un id unico para el reconocimiento de este dentro de la empresa
     *
     */
    public void insertarZapato(String referencia, String sexo, int talla, String marca, String proveedor,
                               Date fechaEntrada, float precio) {
        String sentenciaSql = "INSERT INTO zapato (referencia, sexo, talla, marca, proveedor, fechaEntrada, precio)" +
                "VALUES(?,?,?,?,?,?,?)";

        PreparedStatement sentencia = null;
        int idmarca = Integer.valueOf(marca.split(" ")[0]);
        int idproveedor = Integer.valueOf(proveedor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, referencia);
            sentencia.setString(2, sexo);
            sentencia.setInt(3, talla);
            sentencia.setInt(4, idmarca);
            sentencia.setInt(5, idproveedor);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fechaEntrada)));
            sentencia.setFloat(7, precio);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }



     // Inserta los datos de Marca de los zapatos, todos los parametros
    void insertarMarca(String marca, String modelo, String color) {
        String sentenciaSql = "INSERT INTO marca (marca, modelo, color)" +
                " VALUES(?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, color);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }


    // Inserta los datos de los proveedores de los zapatos, todos los parametros
    void insertarProveedor(String nombre, String direccion, String email, int telefono, String web) {
        String sentenciaSql = "INSERT INTO proveedor(nombre, direccion, email, telefono, web)" +
                "VALUES(?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, email);
            sentencia.setInt(4, telefono);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }

    }


    /*
    *   Estos 3 metodos, llamados modificarZapato, modificarMarca y modificarProveedor permite modificar
    *   en la tabla la casilla donde aparezca el parametro que se haya introducido previamente
    *
        *   @param referencia del zapato, tendra letras y numeros
        *   @param sexo para saber si sera de hombre o de mujer el zapato
        *   @param talla numero de talla del cliente
        *   @param marca marca que se elegira con el combobox
        *   @param proveedor el proveedor que hay en la clase enumerada, tambien se elegira con el combobox
        *   @param fechaEntrada la entrada del zapato en la empresa
        *   @param precio el precio del calzado
        *   @param idzapato al igual que la referencia, se usara un id unico para el reconocimiento de este dentro de la empresa
    *
    * */


    public void modificarZapato(String referencia, String sexo, int talla, String marca, String proveedor,
                                Date fechaEntrada, float precio, int idzapato) {

        String sentenciaSql = "UPDATE zapato SET referencia = ?, sexo = ?, talla = ?, idmarca = ?, idprveedor = ?, " +
                "fechaEntrada = ?, precio = ? WHERE idzapato = ?";
        PreparedStatement sentencia = null;

        int idmarca = Integer.valueOf(marca.split(" ")[0]);
        int idproveedor= Integer.valueOf(proveedor.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, referencia);
            sentencia.setString(2, sexo);
            sentencia.setInt(3, talla);
            sentencia.setInt(4, idmarca);
            sentencia.setInt(5, idproveedor);
            sentencia.setDate(6, Date.valueOf(String.valueOf(fechaEntrada)));
            sentencia.setInt(7, idzapato);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }


    public void modificarMarca(String marca, String modelo, String color, int idmarca){
        String sentenciaSql = "UPDATE marca SET marca = ?, modelo = ?, color = ? WHERE idmarca = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, color);
            sentencia.setInt(4, idmarca);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarProveedor(String nombre, String direccion, String email, int telefono, String web, int idproveedor){
        String sentenciaSql = "UPDATE proveedor SET nombre = ?, direccion = ?, email = ?," +
                "telefono = ?, web = ?,  WHERE idproveedor = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, email);
            sentencia.setInt(4, telefono);
            sentencia.setString(5, web);
            sentencia.setInt(6, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que elimina un zapato
     * @param idzapato id del zapato
     */
    void borrarZapato(int idzapato) {
        String sentenciaSql = "DELETE FROM zapato WHERE idzapato = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idzapato);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que elimina una marca de la base de datos
     * @param idmarca id de la marca
     */
    void borrarMarca( int idmarca) {
        String sentenciaSql = "DELETE FROM marca WHERE idmarca = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idmarca);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que elimina un proveedor
     * @param idproveedor id del proveedor
     */

    void borrarProveedor(int idproveedor) {
        String sentenciaSql = "DELETE FROM proveedor WHERE idproveedor = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idproveedor);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if(sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    /**
     * Metodo que devuelve todos los zapatos de la base de datos
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarZapato() throws SQLException {
        String sentenciaSql = "SELECT concat(z.idzapato) as 'ID', concat(z.referencia) as 'Referencia', " +
                "concat(z.sexo) as 'Sexo', concat(z.talla) as 'Talla', " +
                "concat(m.idmarca, ' - ', m.marca) as 'Marca', concat(b.genero) as 'Género', " +
                "concat(p.idproveedor, ' - ', p.proveedor, ', ', a.nombre) as 'Nombre', " +
                "concat(z.fechaEntrada) as 'Fecha de entrada', concat(z.precio) as 'Precio'" +
                " FROM zapato as z " +
                "inner join marca as e on m.idmarca = z.idmarca inner join " +
                "proveedor as a on p.idproveedor = z.idproveedor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que devuelve las marcas guardados en la bbdd
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarMarca() throws SQLException {
        String sentenciaSql = "SELECT concat(idmarca) as 'ID', concat(marca) as 'Marca', concat(Modelo) as 'Modelo', " +
                "concat(color) as 'Color' FROM marca";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que devuelve los proveedores guardados en la bbdd
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarProveedor() throws SQLException {
        String sentenciaSql = "SELECT concat(idproveedor) as 'ID', concat(nombre) as 'Nombre', concat(direccion) as 'Direccion', " +
                "concat(email) as 'Email', concat(telefono) as 'Telefono', concat(web) as 'Web' FROM proveedor";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }


    //Lee el archivo de propiedades y devuelve los atributos pertinentes
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
        void setPropValues (String ip, String user, String pass, String adminPass) {
            try {
                Properties prop = new Properties();
                prop.setProperty("ip", ip);
                prop.setProperty("user", user);
                prop.setProperty("pass", pass);
                prop.setProperty("admin", adminPass);
                OutputStream out = new FileOutputStream("config.properties");
                prop.store(out, null);

            } catch (IOException ex) {
                ex.printStackTrace();
            }
            this.ip = ip;
            this.user = user;
            this.password = pass;
            this.adminPassword = adminPass;
        }

    /**
     * Este metodo comprueba si la referencia de un zapato ya esta en la base de datos
     * @param referencia la referencia de un zapato
     * @return true si ya existe
     */
    public boolean referenciaZapatoSiExiste(String referencia) {
        String consultaRef = "SELECT existeReferencia(?)";
        PreparedStatement function;
        boolean referenciaExists = false;
        try {
            function = conexion.prepareStatement(consultaRef);
            function.setString(1, referencia);
            ResultSet rs = function.executeQuery();
            rs.next();

            referenciaExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return referenciaExists;
    }

    /**
     *   Metodo que comprueba si el modelo de la marca ya existe en la base de datos
     * @param modelo modelo de la marca
     * @return true si ya existe
     */
    public boolean modeloMarcaYaExiste(String modelo) {
        String consultaModelo = "SELECT existeModeloMarca(?)";
        PreparedStatement function;
        boolean modeloExists = false;
        try {
            function = conexion.prepareStatement(consultaModelo);
            function.setString(1, modelo);
            ResultSet rs = function.executeQuery();
            rs.next();

            modeloExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return modeloExists;
    }

    /**
     * Metodo comprueba si el nombre del proveedor ya existe en la base de datos
     * @param nombre nombre del proveedor
     * @return true si ya existe
     */
    public boolean nombreTelefonoProveedorYaExiste(String nombre, int telefono) {
        String contacto = nombre + ", " + telefono;
        String consultaNombre = "SELECT existeNombreProveedor(?)";
        PreparedStatement function;
        boolean nombreExists = false;
        try {
            function = conexion.prepareStatement(consultaNombre);
            function.setString(1, contacto);
            ResultSet rs = function.executeQuery();
            rs.next();

            nombreExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nombreExists;
    }


    }



