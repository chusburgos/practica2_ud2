package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Main {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista(tabbedPane1, etiquetaEstado);
        Controlador controlador = new Controlador(modelo, vista);
    }
}
