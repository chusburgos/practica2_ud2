package base.enums;
/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox comboTiposProveedores de la vista.
 * Representan los de proveedores que existen.
 */
public enum TiposProveedores {

    ULANKA("Ulanka"),
    NIKEFACTORY("Nike Factory"),
    ELCORTEINGLES("El corte ingles"),
    PABLOOCHOA("Pablo Ochoa"),
    CARLOSREULA("CarlosReula"),
    WOMANBOUTIQUE("Woman Boutique"),
    GALLERYCARRILE("Gallery Carrile"),
    FARRUTX("Farrutx");

    private String valor;

    TiposProveedores(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }

}
