package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos desde un fichero, añade los listeners y lista los datos
     * @param modelo Modelo
     * @param vista Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarZapatos();
        refrescarMarcas();
        refrescarProveedor();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.anadirMarcaBtn.addActionListener(listener);
        vista.anadirMarcaBtn.addActionListener(listener);
        vista.anadirProveedorBtn.addActionListener(listener);

        vista.eliminarBtn.addActionListener(listener);
        vista.eliminarMarcaBtn.addActionListener(listener);
        vista.eliminarProveedorBtn.addActionListener(listener);

        vista.modificarBtn.addActionListener(listener);
        vista.modificarMarcaBtn.addActionListener(listener);
        vista.modificarProveedorBtn.addActionListener(listener);

        vista.optionDialog.guardarBtn.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }
    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {

            if (e.getSource().equals(vista.tablaZapatos.getSelectionModel())) {
                int row = vista.tablaZapatos.getSelectedRow();
                vista.referenciaTxt.setText(String.valueOf(vista.tablaZapatos.getValueAt(row, 1)));
                vista.sexoTxt.setText(String.valueOf(vista.tablaZapatos.getValueAt(row, 2)));
                vista.tallaTxt.setText(String.valueOf(vista.tablaZapatos.getValueAt(row, 3)));
                vista.comboMarca.setSelectedItem(String.valueOf(vista.tablaZapatos.getValueAt(row, 4)));
                vista.comboProveedor.setSelectedItem(String.valueOf(vista.tablaZapatos.getValueAt(row, 5)));
                vista.fecha.setDateTimeStrict(LocalDateTime.from((Date.valueOf(String.valueOf(vista.tablaZapatos.getValueAt(row, 3)))).toLocalDate()));
                vista.precioTxt.setText(String.valueOf(vista.tablaZapatos.getValueAt(row, 7)));
            }

            else if (e.getSource().equals(vista.tablaMarcas.getSelectionModel())) {
                int row = vista.tablaMarcas.getSelectedRow();
                vista.comboNombreMarca.setSelectedItem(String.valueOf(vista.tablaMarcas.getValueAt(row, 1)));
                vista.modeloTxt.setText(String.valueOf(vista.tablaMarcas.getValueAt(row, 2)));
                vista.colorTxt.setText(String.valueOf(vista.tablaMarcas.getValueAt(row, 4)));
            }

            else if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {
                int row = vista.tablaProveedor.getSelectedRow();
                vista.comboNombreProveedor.setSelectedItem(String.valueOf(vista.tablaProveedor.getValueAt(row, 1)));
                vista.direccionTxt.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 5)));
                vista.emailTxt.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 3)));
                vista.telefonoTxt.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 4)));
                vista.webTxt.setText(String.valueOf(vista.tablaProveedor.getValueAt(row, 5)));

            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tablaZapatos.getSelectionModel())) {
                    borrarCamposZapatos();
                } else if (e.getSource().equals(vista.tablaMarcas.getSelectionModel())) {
                    borrarCamposMarcas();
                } else if (e.getSource().equals(vista.tablaProveedor.getSelectionModel())) {
                    borrarCamposProveedores();
                }
            }
        }
    }

    /**
     *  Añade las acciones a los botones de añadir, modificar y eliminar de las listas.
     *  Añade las acciones a los menus de guardar, guardar como y mostrar opciones.
     *  Añade la acción de guardar las opciones al dialog de opciones.
     *  Añade las acciones de guardar, no guardar y cancelar a los botones del dialog de guardar
     *  cambios cuando se intenta salir de la aplicación una vez se ha desactivado el autoguardado.
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida es incorrecta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIp.getText(), vista.optionDialog.tfUsuario.getText(),
                        String.valueOf(vista.optionDialog.pfCont.getPassword()), String.valueOf(vista.optionDialog.pfContAdm.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "anadirZapato": {
                try {
                    if (comprobarZapatoVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaZapatos.clearSelection();
                    } else if (modelo.referenciaZapatoSiExiste(vista.referenciaTxt.getText())) {
                        Util.showErrorAlert("La referencia ya esta registrada.\nIntroduce un nuevo zapato");
                        vista.tablaZapatos.clearSelection();
                    } else
                        modelo.insertarZapato(
                                vista.referenciaTxt.getText(),
                                vista.sexoTxt.getText(),
                                Integer.parseInt(vista.tallaTxt.getText()),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboProveedor.getSelectedItem()),
                                Date.valueOf(String.valueOf(vista.fecha.getDatePicker())),
                                Float.parseFloat(vista.precioTxt.getText()));

                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaZapatos.clearSelection();
                }
                borrarCamposZapatos();
                refrescarZapatos();
            }
            break;
            case "modificarZapato": {
                try {
                    if (comprobarMarcaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaZapatos.clearSelection();
                    } else
                        modelo.modificarZapato(
                                vista.referenciaTxt.getText(),
                                vista.sexoTxt.getText(),
                                Integer.parseInt(vista.tallaTxt.getText()),
                                String.valueOf(vista.comboMarca.getSelectedItem()),
                                String.valueOf(vista.comboProveedor.getSelectedItem()),
                                Date.valueOf(String.valueOf(vista.fecha.getDatePicker())),
                                Float.parseFloat(vista.precioTxt.getText()),
                                Integer.parseInt((String)vista.tablaZapatos.getValueAt(vista.tablaZapatos.getSelectedRow(), 0)));

                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaZapatos.clearSelection();
                }
                borrarCamposZapatos();
                refrescarZapatos();
            }
            break;

            case "eliminarZapato":
                modelo.borrarZapato(Integer.parseInt((String)vista.tablaZapatos.getValueAt(vista.tablaZapatos.getSelectedRow(), 0)));
                borrarCamposZapatos();
                refrescarZapatos();
                break;


            case "anadirMarca": {
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaMarcas.clearSelection();
                    } else if (modelo.modeloMarcaYaExiste(vista.modeloTxt.getText())) {
                        Util.showErrorAlert("Ese modelo ya existe.\nIntroduce un modelo diferente");
                        vista.tablaMarcas.clearSelection();
                    } else {
                        modelo.insertarMarca(
                                String.valueOf(vista.comboNombreMarca.getSelectedItem()),
                                vista.modeloTxt.getText(),
                                vista.colorTxt.getText());
                        refrescarMarcas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaMarcas.clearSelection();
                }
                borrarCamposMarcas();
            }
            break;
            case "modificarMarca": {
                try {
                    if (comprobarMarcaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaMarcas.clearSelection();
                    } else {
                        modelo.modificarMarca(
                                String.valueOf(vista.comboNombreMarca.getSelectedItem()),
                                vista.modeloTxt.getText(),
                                vista.colorTxt.getText(),
                                Integer.parseInt((String)vista.tablaMarcas.getValueAt(vista.tablaMarcas.getSelectedRow(), 0)));
                        refrescarMarcas();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaMarcas.clearSelection();
                }
                borrarCamposMarcas();
            }
            break;
            case "eliminarMarca":
                modelo.borrarMarca(Integer.parseInt((String)vista.tablaMarcas.getValueAt(vista.tablaMarcas.getSelectedRow(), 0)));
                borrarCamposMarcas();
                refrescarMarcas();
                break;


            case "anadirProveedor": {
                try {
                    if (comprobarProveedorVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProveedor.clearSelection();
                    } else if (modelo.telefonoProveedorYaExiste(Integer.parseInt(vista.telefonoTxt.getText()))) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una editorial diferente.");
                        vista.tablaProveedor.clearSelection();
                    } else {
                        modelo.insertarProveedor(
                                String.valueOf(vista.comboNombreProveedor.getSelectedItem()),
                                vista.direccionTxt.getText(),
                                vista.emailTxt.getText(),
                                Integer.parseInt(vista.telefonoTxt.getText()),
                                vista.webTxt.getText());
                        refrescarProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaProveedor.clearSelection();
                }
                borrarCamposProveedores();
            }
            break;
            case "modificarProveedor": {
                try {
                    if (comprobarProveedorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaProveedor.clearSelection();
                    } else {
                        modelo.modificarProveedor(String.valueOf(vista.comboNombreProveedor.getSelectedItem()),
                                vista.direccionTxt.getText(),
                                vista.emailTxt.getText(),
                                Integer.parseInt(vista.telefonoTxt.getText()),
                                vista.webTxt.getText(),
                                Integer.parseInt((String)vista.tablaProveedor.getValueAt(vista.tablaProveedor.getSelectedRow(), 0)));
                        refrescarProveedor();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaProveedor.clearSelection();
                }
                borrarCamposProveedores();
            }
            break;
            case "eliminarProveedor":
                modelo.borrarProveedor(Integer.parseInt((String)vista.tablaProveedor.getValueAt(vista.tablaProveedor.getSelectedRow(), 0)));
                borrarCamposProveedores();
                refrescarProveedor();
                break;
        }
    }

    private void refrescarProveedor() {
        try {
            vista.tablaProveedor.setModel(construirTableModelProveedor(modelo.consultarProveedor()));
            vista.comboNombreProveedor.removeAllItems();
            for(int i = 0; i < vista.dtmProveedores.getRowCount(); i++) {
                vista.comboNombreProveedor.addItem(vista.dtmProveedores.getValueAt(i, 0) + " - " +
                        vista.dtmProveedores.getValueAt(i, 1) + ", " + vista.dtmProveedores.getValueAt(i, 2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelProveedor(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmProveedores.setDataVector(data, columnNames);

        return vista.dtmProveedores;

    }


    private void refrescarZapatos() {
        try {
            vista.tablaZapatos.setModel(construirTableModelZapatos(modelo.consultarZapato()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelZapatos(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmZapatos.setDataVector(data, columnNames);

        return vista.dtmZapatos;
    }


    /**
     * Actualiza las marcas de los zapartos que se ven en la lista y los comboboxs
     */

    private void refrescarMarcas() {
        try {
            vista.tablaMarcas.setModel(construirTableModelMarcas(modelo.consultarMarca()));
            vista.comboNombreMarca.removeAllItems();
            for(int i = 0; i < vista.dtmMarcas.getRowCount(); i++) {
                vista.comboNombreMarca.addItem(vista.dtmMarcas.getValueAt(i, 0) + " - " +
                        vista.dtmMarcas.getValueAt(i, 2) + ", " + vista.dtmMarcas.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelMarcas(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmMarcas.setDataVector(data, columnNames);

        return vista.dtmMarcas;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }


    /**
     * Comprueba que los campos necesarios para añadir un zapato estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarZapatoVacio() {
        return vista.referenciaTxt.getText().isEmpty() ||
                vista.sexoTxt.getText().isEmpty() ||
                vista.tallaTxt.getText().isEmpty() ||
                vista.comboMarca.getSelectedIndex() == -1 ||
                vista.comboProveedor.getSelectedIndex() == -1 ||
                vista.fecha.datePicker.isDateAllowed(null) ||
                vista.precioTxt.getText().isEmpty();
    }


    private boolean comprobarProveedorVacia() {
        return vista.comboNombreProveedor.getSelectedIndex() == -1 ||
                vista.direccionTxt.getText().isEmpty() ||
                vista.emailTxt.getText().isEmpty() ||
                vista.telefonoTxt.getText().isEmpty() ||
                vista.webTxt.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una marca estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarMarcaVacia() {
    return vista.comboNombreMarca.getSelectedIndex() == -1 ||
            vista.modeloTxt.getText().isEmpty() ||
            vista.colorTxt.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un proveedor estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarProveedorVacio() {
    return vista.comboNombreProveedor.getSelectedIndex() == -1 ||
            vista.direccionTxt.getText().isEmpty() ||
            vista.emailTxt.getText().isEmpty() ||
            vista.telefonoTxt.getText().isEmpty() ||
            vista.webTxt.getText().isEmpty();
    }

    /**
     * Vacía los campos de la tabla zapato
     */
    private void borrarCamposZapatos() {
        vista.referenciaTxt.setText("");
        vista.sexoTxt.setText("");
        vista.tallaTxt.setText("");
        vista.comboMarca.setSelectedIndex(-1);
        vista.comboProveedor.setSelectedIndex(-1);
        vista.fecha.setDateTimeStrict(null);
    }

    /**
     * Vacía los campos de la tabla marca
     */
    private void borrarCamposMarcas() {
        vista.comboNombreMarca.setSelectedIndex(-1);
        vista.modeloTxt.setText("");
        vista.colorTxt.setText("");
    }

    /**
     * Vacía los campos de la tabla proveedor
     */
    private void borrarCamposProveedores() {
        vista.comboNombreProveedor.setSelectedIndex(-1);
        vista.direccionTxt.setText("");
        vista.emailTxt.setText("");
        vista.telefonoTxt.setText("");
        vista.webTxt.setText("");
    }



    /**
     * Devuelve el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIp.setText(modelo.getIP());
        vista.optionDialog.tfUsuario.setText(modelo.getUser());
        vista.optionDialog.pfCont.setText(modelo.getPassword());
        vista.optionDialog.pfContAdm.setText(modelo.getAdminPassword());
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {

    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {

    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {

    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {

    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {

    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {

    }


}
