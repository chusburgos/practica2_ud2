package gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog {
    private JPanel panel1;
    JTextField tfIp;
    JTextField tfUsuario;
    public JButton guardarBtn;
    JPasswordField pfCont;
    JPasswordField pfContAdm;
    private Frame owner;

    public OptionDialog(Frame owner) {
        super(owner, "opciones", true);
        setContentPane(panel1);
        this.owner = owner;
        initDialog();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight()));
        this.setLocationRelativeTo(owner);
    }
}