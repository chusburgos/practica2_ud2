CREATE DATABASE if not exists zapWalkShoes;

USE zapWalkShoes;

create table if not exists zapato (
idzapato int auto_increment primary key,
referencia varchar(40) not null UNIQUE,
sexo varchar(15) not null,
talla int not null,
fechanacimiento date,
idmarca int not null, 
idproveedor int not null,
stock int not null,
precio float not null);
--
create table if not exists marca (
idmarca int auto_increment primary key,
marca varchar(50) not null,
modelo varchar(100) not null,
color varchar(20) not null);
--
create table if not exists proveedor (
idproveedor int auto_increment primary key,
nombre varchar(50) not null,
Direccion varchar(100) not null,
Email varchar(100) not null, 
Telefono int not null,
web varchar(100) not null);
--
alter table zapato 
	add foreign key(idmarca) references marca(idmarca),
    add foreign key(idproveedor) references proveedor(idproveedor);
--
create function existeReferencia(funcion_referencia varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    while(i < select max(idzapato from zapato)) do
    if((select referencia from zapato where idzapato = (i + 1) like funcion_referencia) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
end;
